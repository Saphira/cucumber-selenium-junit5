Feature: Search
  As a user
  I want to search
  so that I can find information
Scenario: Search info about BDD
  Given I am on the Ecosia search page
  When I search for "BDD"
  Then the page title should start with "BDD"