Feature: Login into the application
    As a not logged in user,
    I would like to log into the application,
    So that I can use the application
Scenario:Test 1: Login with valid credentials
    Given I am on the application login page
    When I type in "opensourcecms" in the "Username" field
    And I type in "opensourcecms" in the "Password" field
    And I click on the "Log in" button
    Then the home page is visible
