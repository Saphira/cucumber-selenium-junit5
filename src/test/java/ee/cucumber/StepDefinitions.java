package ee.cucumber;

import ee.cucumber.ecosia.pages.EcosiaSearchPage;
import ee.cucumber.ohrm.utils.TestTemplate;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static ee.cucumber.ohrm.utils.TestCommons.BASE_URL;
import static ee.cucumber.ohrm.utils.TestCommons.TITLE_HOME;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StepDefinitions extends TestTemplate {
    private static EcosiaSearchPage objEcosiaSearchPage;
    @Before
    public void setup(){ set(); }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Given("I am on the application login page")
    public void openHomePage() {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    @When("I type in {string} in the \"Username\" field")
    public void insertUser(String user){

        driver.findElement(By.cssSelector("#divUsername > .form-hint")).click();
        driver.findElement(By.id("txtUsername")).sendKeys(user);

    }

    @When("I type in {string} in the \"Password\" field")
    public void insertPassword(String password){

        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys(password);

    }
    @When("I click on the \"Log in\" button")
    public void submitLoginForm(){
        driver.findElement(By.id("btnLogin")).click();
    }

    @Then("the home page is visible")
    public void checkHomePage(){
        wait.until(ExpectedConditions.titleIs(TITLE_HOME));
        assertTrue(driver.getTitle().equals(TITLE_HOME));
    }

    @Given("I am on the Ecosia search page")
    public void openEcosiaSearchPage() {
        driver.get("https://www.ecosia.org/");
    }

    @When("I search for {string}")
    public void search(String text) {
        objEcosiaSearchPage = new EcosiaSearchPage(driver);

        objEcosiaSearchPage.setQuery(text);
        objEcosiaSearchPage.search();
    }

    @Then("the page title should start with {string}")
    public void thePageTitleShouldStartWith(String titleStartsWith) {
        assertTrue(driver.getTitle().contains(titleStartsWith));
    }
}
